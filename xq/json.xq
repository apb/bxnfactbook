(: return json :)
import module namespace app="apb.factbook" ;
import module namespace state="apb.state" ;
declare namespace model="apb.model";

declare option output:method "json";
declare variable $svg:=db:open("svg","bluesq.svg");
declare variable $_req external;
declare variable $req:= json:parse($_req);
let $page:= model:json(map{
                    "title":="JSON Factbase",
                    "content":=$svg,
                    "req":=$req
                    })
return $page

