(:~
: show schema as xml
:)
import module namespace app="apb.factbook" ;
import module namespace model="apb.model" ;

declare option output:method "json";

let $s:=model:xml2html($app:schema)
let $page:= model:json(map{
                    "title":="Factbase schema"
                   ,"content":= $s             
                    })
return $page
