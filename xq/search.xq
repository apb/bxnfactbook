(:~
: search form
:)
import module namespace app="apb.factbook";
import module namespace state="apb.state" ;
import module namespace model="apb.model";

declare option output:method "json";

declare variable $_req external;
declare variable $req:= json:parse($_req);

let $q:=string($req/query/q)
let $res:=
   <ol>{
  for $hit in $app:factbook//*[@name] | $app:factbook//city
          
           let score $score:= $hit/@name contains text ({$q} using fuzzy weight{0.4}) 
                          or  $hit/name contains text ({$q} using fuzzy weight{0.6})
           let $id:=$hit/@id
           let $name:=($hit/@name,$hit/name)[1]
           where $score gt 0           
           order by $score descending
           return  <li>
                <a href="fb/{name($hit)}/{$id}">{concat($name,' (',name($hit),' ',$id,')')}</a>
                 <span> {format-number($score*100,'00')}%</span>
                </li>
    }</ol>

let $result:=model:xml2html($res)  

let $s:=<div> <form action="search" class="form">
                   <label for="q">Search:</label>
                    <input id="q" type="text" value="{$q}" name="q"/>
                  </form> <hr/>{($res,<hr/>,$result)}</div>
let $page:= model:json(map{
                    "title":="Factbase search"
                   ,"content":= $s
                    })
return $page              
