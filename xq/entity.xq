(:~
: display item or items
: @param entity eg 
:)
import module namespace app="apb.factbook" ;
import module namespace state="apb.state" ;
declare namespace model="apb.model";

declare option output:method "json";

declare variable $id external;
declare variable $entity external;
declare variable $repeat external;
declare variable $_req external;
declare variable $req:= json:parse($_req);

let $base:=$req/url/pathname
let $root:=substring($base,1+string-length($req/basepath))
let $sort:=string($req/query/sort)

let $page:=if(""=$id)
           then app:list-page($entity,$sort,$req )
           else if(""=$repeat)
           then app:detail-page($entity,$id,$req,$root)
           else app:repeat-page($entity,$id,$repeat,$req)
 
return model:json($page) 
 
