(:~
: about
:)
import module namespace app="apb.factbook" ;
declare namespace model="apb.model";

declare option output:method "json";

declare variable $_req external;

let $content:=  <div>
          <div class="alert alert-error">
            <p><strong>World Factbook</strong> This website is a demonstration of
            the use of BaseX and Node to display information from the
            mondial dataset. The mondial dataset is based on the
           <a href="https://www.cia.gov/library/publications/the-world-factbook/" target="_blank">
             CIA factbook</a>.</p>
           
            </div>
            <ul>
            <li><a href="https://bitbucket.org/apb/bxnfactbook" target="source">Bitbucket</a></li>
            <li><a href="https://twitter.com/mrwhere4" target="twitter">Twitter</a></li>
            </ul>
    </div>
let $page:= model:json(map{
                    "title":="About Factbase"
                   ,"content":= $content
                   ,"user":=<div>User</div>  
                    })
return $page
