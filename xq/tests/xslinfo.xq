let $in :=<dummy/>
let $style :=
<xsl:stylesheet version="2.0" xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
                        <xsl:template match="/">
                            <data type="xsl">
                                <item name="version" value="{system-property('xsl:version')}" />
                                <item name="vendor" value="{system-property('xsl:vendor')}" />
                                <item name="vendor-url" value="{system-property('xsl:vendor-url')}" />
                                
                            </data>
                        </xsl:template>
                    </xsl:stylesheet>
 return xslt:transform($in, $style)                   