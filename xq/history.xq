(:~
: show usage for session id
:)
import module namespace app="apb.factbook";
import module namespace state="apb.state" ;
import module namespace model="apb.model";

declare option output:method "json";

declare variable $_req external;
declare variable $req:= json:parse($_req);

declare variable $sid:=string($req/session);

let $seq:=$state:sessions/session[@sid=$sid]//hit
let $entity:="hit"
let $sort:="-ts"
let $schema:=$app:schema/db/entity[@name=$entity]

let $sorted:=model:sort($seq,$sort,$schema)
let $fields:=$schema/attributes/field
let $fmap:=model:access-map($schema)
let $table:=app:sequence-to-table3($sorted,$fields,$fmap,$req)
let $page:= model:json(map{
                    "title":="History this session"
                   ,"content":= $table
                    })
return $page     
