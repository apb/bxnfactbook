(:
: display schema svg 
:)
import module namespace app="apb.factbook" ;
declare namespace model="apb.model";

declare option output:method "json";


declare variable $_req external;

let $svg:=db:open("factbook","schema.svg")
let $page:= model:json(map{
                    "title":="Factbase home"
                   ,"content":=<div style="width:90%;height:30em">
                           {$svg}
                           </div>
                   ,"user":=<div>User</div>  
                    })
return $page
