(:~
: show request detail debug tool
:)
import module namespace app="apb.factbook";
import module namespace state="apb.state" ;
import module namespace model="apb.model";

declare option output:method "json";


declare variable $_req external;
declare variable $req:= json:parse($_req);

let $s:=model:xml2html($req)

let $page:= model:json(map{
                    "title":="Show request"
                   ,"content":= $s 
                    })
return $page




