// useful server pages
// mount as a group
var fs = require('fs');

function session_view(req, res) {
	// test session
	req.session.visitCount = req.session.visitCount ? req.session.visitCount + 1
			: 1;

	res.writeHead(200, {
		'Content-Type' : 'text/plain'
	});
	res.write('You have visited this page ' + req.session.visitCount
			+ ' times. ');
	res.write(JSON.stringify(req.session, true, 2));
	res.end();
};


function page404(req, res) {
	res.send('This page can not be found. Status 404!', 404);
};

function simple(request, response) {
	response.writeHead(200, {
		'Content-Type' : 'text/plain'
	});
	response.write('Hello at' + new Date());
	response.end();
};

function request_view(request, response) {
	// show headers
	var h = JSON.stringify(request.headers, true, 2);
	// var h = h+JSON.stringify(request.connection, true, 2);
	response.writeHead(200, {
		'Content-Type' : 'text/plain'
	});
	response.write('Request: ' + h);
	response.end();
};

function post_view(request, response) {
	// show post body
	var h = JSON.stringify(request.body, true, 2);
	response.writeHead(200, {
		'Content-Type' : 'text/plain'
	});
	response.write('Request: ' + h);
	response.end();
};

function mountRoute(app, mount) {
	var routes = function(request, response) {
		// return page describing all routes
		var r2 = allroutes(app, false);
		response.render("route.ejs", {
			locals : {
				routes : r2,
				title : "All routes"
			}
		});
	};
	app.get(mount, routes);
};

exports.mount = function(app, mount) {
	// setup routes
	mountRoute(app, mount + '/routes');
	app.get(mount + '/simple', simple);
	app.get(mount + '/request', session_view);
	app.get(mount + '/session', session_view);
	app.get(mount + '/post', post_view);
	app.get(mount + '/packages', npm);
};

function npm(request, response) {
	// show package
	// Force encoding to ascii text
	fs.readFile(__dirname + '/package.json', 'ascii', function(err, data) {
		if (err) {
			console.error("Could not open file: %s", err);
			process.exit(1);
		}
		var p = JSON.parse(data)
		response.render('npm.ejs', {
			layout : true,
			locals : {
				title : "package.json",
				result : data,
				deps : p["dependencies"]
			}
		});
	});
};

function allroutes(app, sortpath) {
	// @param sortpath boolean
	// @return array of route objects
	var r2 = [];
	app.routes.all().forEach(function(route, index) {
		r2.push({
			method : route.method.toUpperCase(),
			path : route.path,
			index : index + 1
		})
	});
	if (sortpath) {
		r2.sort(function(a, b) {
			var r = a.path.localeCompare(b.path);
			if (0 == r) {
				r = a.method.localeCompare(b.method)
			}
			return r;
		});
	}
	return r2;
};

//get path from request
function baseuri(basepath) {
	return function(req, res) {
		var base = req.headers["x-forwarded-host"];
		if (!base)
			base = req.headers.host;
		return "http://" + base + basepath;
	}
};

exports.session_view = session_view;
exports.post_view = post_view;
exports.request_view = request_view;
exports.simple = simple;
exports.page404 = page404;
exports.baseuri = baseuri;
