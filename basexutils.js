// utilities for basex
// list,info,help
//
var basex = require("basex");
var session = new basex.Session();

exports.addRoutes = function(app,mount) {
	app.get(mount+'/', index);
	app.get(mount+'/databases', databases);
	app.get(mount+'/packages', packages);
	app.get(mount+'/commands', cmds);
	app.get(mount+'/query', query);
	app.post(mount+'/query', querypost);
};
function querypost(req, res){
	var data = req.body;
	session.execute(data,function(err,reply){
	res.render('basex/query.ejs', {
		layout : true,
		locals : {
			title : "BaseX query",
			xresult : "result: "+ reply.result
		}
	});
	})
};

function query(req, res){
	
	res.render('basex/query.ejs', {
		layout : true,
		locals : {
			title : "BaseX query",
			xresult : ""
		}
	});
};

function cmds(req, res){
	var cmd="help";
	//console.dir(req.headers);
	session.execute(cmd, function(err, r) {
	res.render('basex/index.ejs', {
		layout : true,
		locals : {
			title : "BaseX cmds",
			result : r.result
		}
	});
	});
};
function packages(req, res){
	var cmd="repo list";
	//console.dir(req.headers);
	session.execute(cmd, function(err, r) {
	res.render('basex/index.ejs', {
		layout : true,
		locals : {
			title : "BaseX packages",
			result : r.result
		}
	});
	});
};
function databases(req, res){
	var cmd="list";
	//console.dir(req.headers);
	session.execute(cmd, function(err, r) {
	res.render('basex/index.ejs', {
		layout : true,
		locals : {
			title : "BaseX databases",
			result : r.result
		}
	});
	});
};
function index(req, res){
	var cmd=req.param("cmd","info");
	//console.dir(req.headers);
	session.execute(cmd, function(err, r) {
	res.render('basex/index.ejs', {
		layout : true,
		locals : {
			title : "BaseX",
			result : r.result
		}
	});
	});
};


