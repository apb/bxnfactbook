// factbook core
var basex = require("basex");
var url = require("url");
var ql = require("./lib/queryloader.js");
var session = new basex.Session();
// basex.debug_mode=true;

// cache xquery files
ql.load(__dirname + "/xq/", [ "entity.xq", "search.xq", "updatelog.xq",
		"showlog.xq", "showrequest.xq", "home.xq", "about.xq", "history.xq",
		"schema.xq", "json.xq" ]);

// create query and bind values from the request
function makeq(name, req) {
	var q = session.query(ql.get(name));
	if (req) {
		var _req = {
			headers : req.headers,
			url : url.parse(req.url),
			method : req.method,
			query : req.query,
			user : req.user,
			session : req.session.id
		};
		q.bind("_req", JSON.stringify(_req));
	}
	;
	return q;
};

function undef(v) {
	return (v === undefined) ? "" : v
}

function request(req, res) {
	// console.log("qqq",query2);
	var query = makeq("showrequest.xq", req);
	jsonfromQuery(req, res, query);
	logreq(req);
};

function entity(req, res) {
	var q = makeq("entity.xq", req);
	// bind variable
	q.bind("repeat", undef(req.params.repeat));
	q.bind("entity", undef(req.params.entity));
	q.bind("id", undef(req.params.id));
	jsonfromQuery(req, res, q);
	logreq(req);
};

// log the request
function logreq(req) {
	var q3 = makeq("updatelog.xq", req);
	q3.execute(function(err, r) {
		if (err)
			throw err;
		// console.log("log",r);
	});

	q3.close();
};

// show usage
function sessions(req, res) {
	var query = makeq("showlog.xq", req);
	jsonfromQuery(req, res, query);
	logreq(req);
};
// show usage
function usage(req, res) {
	var query = makeq("showlog.xq", req);
	jsonfromQuery(req, res, query);
	logreq(req);
};

// show history
function history(req, res) {
	var query = makeq("history.xq", req);
	jsonfromQuery(req, res, query);
	logreq(req);
};
// respond with html from query
function htmlfromQuery(req, res, query) {
	query.execute(function(err, r) {
		if (err) {
			errorResponse(req, res, query, err)
		} else {
			res.writeHead(200, {
				"Content-Type" : "text/html"
			});
			// console.log(r);
			res.write(r.result);
			res.end();
		}
	});
	query.close();
};

// error from Basex
function errorResponse(req, res, query, err) {
	res.render('message.ejs', {
		layout : true,
		locals : {
			title : "Factbook problem",
			result : "what?",
			errorMessage : err
		}
	});
	console.log("err", err, "query:", query.query)
};

// respond with html from query
function jsonfromQuery(req, res, query) {
	var ts1 = new Date();
	query.execute(function(err, r) {
		if (err) {
			errorResponse(req, res, query, err)
		} else if(r.ok==false){
			errorResponse(req, res, query,r.info)
		}else{
			var j = JSON.parse(r.result);
			j.title += " ms:" + (new Date() - ts1);
			res.render('content.ejs', {
				layout : true,
				locals : j
			});
		}
	});
	query.close();
};

function search(req, res) {
	// console.log("qqq",query2);
	var query = makeq("search.xq", req);
	jsonfromQuery(req, res, query);
	logreq(req);
};

function home(req, res) {
	// console.log("qqq",query2);
	var query = makeq("home.xq", req);
	jsonfromQuery(req, res, query);
	logreq(req);
};

function about(req, res) {
	// console.log("qqq",query2);
	var query = makeq("about.xq", req);
	jsonfromQuery(req, res, query);
	logreq(req);
};

function schema(req, res) {
	// console.log("qqq",query2);
	var query = makeq("schema.xq", req);
	jsonfromQuery(req, res, query);
	logreq(req);
};

function page404(req, res) {
	// console.log("qqq",query2);
	res.render('page404.ejs', {
		layout : true,
		locals : {
			title : "Page not found",
			url : req.url.href
		}
	});
};

function users(req, res) {
	var query = makeq("users.xq", req);
	jsonfromQuery(req, res, query);
	logreq(req);
};
// return user object
function user(req, res) {
	return req.user
};


function json(req, res) {
	var query = makeq("json.xq", req);
	// bind variable
	jsonfromQuery(req, res, query);
	logreq(req);
};

exports.usage = usage;
exports.sessions = sessions;
exports.history = history;
exports.request = request;
exports.home = home;
exports.entity = entity;
exports.search = search;
exports.about = about;
exports.schema = schema;
exports.page404 = page404;
exports.users = users;
exports.user = user;
exports.json = json;