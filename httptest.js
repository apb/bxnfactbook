// basex factbook demo
// apb oct 2011

var conf = require("./config-app.js").state;
var auth = require('./auth.js');

var express = require("express");
var x = require('express-namespace');

var mondial = require("./mondial.js");
var config = require("./config-express.js");
var base_server = require("./base_server.js");

var basexutils = require("./basexutils.js");
// Global app
app = express.createServer();
config.config(app, conf.basepath);
app.dynamicHelpers({
	messages : require('./lib/bootstrap2-flash'),
	base : base_server.baseuri(conf.basepath),
	user: mondial.user
});

auth.use(app,conf.basepath)
// -----------------------
app.namespace(conf.basepath, function() {

	app.get("/", mondial.home);
	basexutils.addRoutes(app, "/basex");
	base_server.mount(app, "base");
 	auth.addRoutes(app, "");
	app.get("/usage", mondial.usage);
	app.get("/sessions", mondial.sessions);
	app.get("/history", auth.andRestrictToLogin, mondial.history);
	app.get("/request", mondial.request);
	app.get("/json", mondial.json);
	app.get("/search", mondial.search);
	app.get("/fb/:entity", mondial.entity);
	app.get("/fb/:entity/:id", mondial.entity);
	app.get("/fb/:entity/:id/:repeat", mondial.entity);
	app.get("/about", mondial.about);
	app.get("/schema", mondial.schema);
	app.get('*', mondial.page404);
});

app.listen(conf.port);

console.log('Server running at port: ', conf);
