// cache for loading xq files
var fs = require('fs');
var assert=require("assert");

var xq = {};

function load(dir,filearray) {
	filearray.forEach(function(file) {
		xq[file] = fs.readFileSync(dir + file, 'utf-8')
	});
	//console.log("loaded");
};
function get(name) {
	assert.ok(xq[name],"xquery not loaded "+name);
	return xq[name];
};
exports.load = load;
exports.get = get;