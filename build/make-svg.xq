(: @TODO :)
let $d:=doc("data/schema.xml")
let $xsl1:=doc("schemagraph.xsl")
let $xsl2:=doc("vizgraph2.xsl")
let $a:=xslt:transform()
xsltproc build/schemagraph.xsl data/schema.xml \
|  xsltproc build/dotml2dot.xsl - \
|  dot -Tsvg \
|  xsltproc build/vizgraph2.xsl - >data/schema.svg
