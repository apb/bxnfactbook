<?xml version="1.0"?>
<!-- entities to svg -->
<xsl:stylesheet version="1.0"
	xmlns="http://www.martin-loetzsch.de/DOTML" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fsm="http://fxom.uki.ime.reuters.com/haveli/modeler/fsm">
	<xsl:output method="xml" omit-xml-declaration="yes" />
	<xsl:template match="/">
		<xsl:variable name="entity" select="//db/entity" />
		<graph rankdir="TB" fontname="Arial">
			<xsl:apply-templates select="$entity" />
			<xsl:apply-templates
				select="$entity//field[(@type='id' or @type='repeat') and @link]" />
		</graph>
	</xsl:template>
	
	<!-- Do Node -->
	<xsl:template match="entity">
		<node shape="ellipse" label="{@name}" id="{@name}" fontname="Arial"
			style="filled" fillcolor="yellow" URL="{concat('fb/',@name)}">
		</node>
	</xsl:template>

	<xsl:template match="field">
		<xsl:variable name="from" select="../../@name" />
		<xsl:variable name="label">
			<xsl:value-of select="@name" />
			<xsl:if test="@type='repeat'">
				<xsl:text>*</xsl:text>
			</xsl:if>
		</xsl:variable>
		<edge label="{$label}" fontname="Arial" from="{$from}" to="{@link}">
		</edge>
	</xsl:template>

</xsl:stylesheet>
