rsync --verbose  --progress --stats --compress  \
      --recursive --times --perms --links --delete \
      --exclude "node_modules/" --exclude "*~" --exclude '.git' \
      * node@velvet:~/BXNfactbook