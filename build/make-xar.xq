(:
create expath xar
:)
declare namespace zip="http://expath.org/ns/zip";
declare namespace pkg="http://expath.org/ns/pkg";

declare variable $pkgpath external; (: ../packages/factbook.xml :)
declare variable $package:=doc($pkgpath)/package;
let $target:=resolve-uri($package/file, $pkgpath)
let $ns:=$package/@name/string()
let $href:=resolve-uri(concat("../",$package/target/string())
                      ,$pkgpath)
let $zip:=
<file xmlns="http://expath.org/ns/zip" href="{substring-after($href,':')}">
    <entry name="expath-pkg.xml">
        <package xmlns="http://expath.org/ns/pkg" name="{$ns}"
            abbrev="lib" version="{$package/@version/string()}" spec="1.0">
            <title>{$package/title/string()}</title>
            <xquery>
                <namespace>{$ns}</namespace>
                <file>{$package/file/string()}</file>
            </xquery>
        </package>
    </entry>
    <dir name="lib">
        <entry src="{substring-after($target,':')}" />
    </dir>
</file>
  
return zip:zip-file($zip)
  