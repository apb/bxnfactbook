(:
: fix up factbook db
:)

(: update file  in db
@param file  
:)
declare updating function local:refresh($file as xs:string)
{
    let $b:=resolve-uri("../data/",static-base-uri())
    let $src:=substring-after(resolve-uri($file,$b),":")
    return  db:replace("factbook",$file,$src) 
};
declare updating function local:ensurelog()
{
     
    (
    if(db:exists("log2","sessions.xml"))
    then ()
    else db:replace("log2", "sessions.xml", "<sessions/>")
    ,
    if(db:exists("log2","users.xml"))
    then ()
    else db:replace("log2", "users.xml", "<users/>")
    )
};

(local:ensurelog()
,local:refresh("schema.xml")
,local:refresh("schema.svg")
,local:refresh("xmlverbatim.xsl")
,local:refresh("factbook.xml")
,db:optimize("factbook")
)

