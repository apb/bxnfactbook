<?xml version="1.0"?>
<!-- tidy up graphviz svg o/p -->
<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/2000/svg"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg"
	xmlns:xlink="http://www.w3.org/1999/xlink">



	<xsl:template match="/svg:svg">
		<xsl:variable name="w" select="substring-before(@width,'pt')" />
		<xsl:variable name="h" select="substring-before(@height,'pt')" />
		<svg width="100%" height="100%" viewBox="0 0 {$w} {$h}"
			preserveAspectRatio="xMidYMid meet" text-rendering="optimizeLegibility">

			<xsl:apply-templates select="*" />
		</svg>
	</xsl:template>
	
	<xsl:template name="copy-content">
		<xsl:apply-templates select="@*|processing-instruction()|*|text()" />
	</xsl:template>
	
	<!--default behaviours for all nodes -->
	<xsl:template match="processing-instruction()|*|@*|text()">
		<xsl:copy>
			<xsl:call-template name="copy-content" />
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
