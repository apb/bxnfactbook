// authneticate using passport.js
var passport = require('passport');
var avatar = require("./lib/avatar");
var conf = require("./config-app.js").state;

var TwitterStrategy = require('passport-twitter').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

var TWITTER_CONSUMER_KEY = "o3Kb8Vew1RHD9vz28Shgg";
var TWITTER_CONSUMER_SECRET = "86CTmvsXARNbfEhaFxlxRAzWNX3pDpSftpRnfaBu0";

var GOOGLE_CLIENT_ID = "177823744754.apps.googleusercontent.com";
var GOOGLE_CLIENT_SECRET = "rgBaey3xyyjQJxdGNopEltFP";

// Passport session setup.
// To support persistent login sessions, Passport needs to be able to
// serialize users into and deserialize users out of the session. Typically,
// this will be as simple as storing the user ID when serializing, and finding
// the user by ID when deserializing. However, since this example does not
// have a database of user records, the complete Twitter profile is serialized
// and deserialized.
passport.serializeUser(function(user, done) {
	console.log("serializeUser", user);
	user.test = "hello";
	done(null, user);
});

passport.deserializeUser(function(obj, done) {
	console.log("deserializeUser", obj);
	done(null, obj);
});
function twitter_callback() {

};

passport.use(new TwitterStrategy({
	consumerKey : TWITTER_CONSUMER_KEY,
	consumerSecret : TWITTER_CONSUMER_SECRET,
	callbackURL : conf.proxy + "auth/twitter/callback"
}, function(token, tokenSecret, profile, done) {
	// asynchronous verification, for effect...
	
	process.nextTick(function() {

		// To keep the example simple, the user's Twitter profile is returned to
		// represent the logged-in user. In a typical application, you would
		// want
		// to associate the Twitter account with a user record in your database,
		// and return that user instead.
		profile.avatar = avatar.get_avatar_from_service("twitter",
				profile.username, 16)
		profile = getUser(profile);
		return done(null, profile);
	});
}));

passport.use(new GoogleStrategy({
	clientID : GOOGLE_CLIENT_ID,
	clientSecret : GOOGLE_CLIENT_SECRET,
	callbackURL : conf.proxy + "auth/google/callback"
}, function(accessToken, refreshToken, profile, done) {
	process.nextTick(function() {
		//var email=profile.emails[0].value;
		profile.username=profile.displayName;
		profile.avatar=avatar.get_avatar_from_service("google",profile.id,16)		
		profile = getUser(profile);
		return done(null, profile);
	});
}));


exports.use = function(app) {
	// Initialize Passport! Also use passport.session() middleware, to support
	// persistent login sessions (recommended).
	app.use(passport.initialize());
	app.use(passport.session());
};

exports.addRoutes = function(app, mount) {
	// Redirect the user to Twitter for authentication. When complete, Twitter
	// will redirect the user back to the application at
	// /auth/twitter/callback
	var redirect = {
		successRedirect : conf.proxy,
		failureRedirect : conf.proxy + 'auth/login'
	};
	app.get(mount + '/auth/twitter', passport.authenticate('twitter'));
	// Twitter will redirect the user to this URL after approval. Finish the
	// authentication process by attempting to obtain an access token. If
	// access was granted, the user will be logged in. Otherwise,
	// authentication has failed.
	app.get(mount + '/auth/twitter/callback', passport.authenticate('twitter',
			redirect));

	app.get(mount + '/auth/google', passport.authenticate('google', {
		scope : [ 'https://www.googleapis.com/auth/userinfo.profile',
				'https://www.googleapis.com/auth/userinfo.email' ]
	}));
	// Twitter will redirect the user to this URL after approval. Finish the
	// authentication process by attempting to obtain an access token. If
	// access was granted, the user will be logged in. Otherwise,
	// authentication has failed.
	app.get(mount + '/auth/google/callback', passport.authenticate('google',
			redirect));

	app.get(mount + '/auth/logout', function(req, res) {
		req.logOut();
		res.redirect(conf.proxy);
	});
	app.get(mount + '/auth/login', function(req, res) {
		req.flash('error', 'Signin required.');
		res.render('login.ejs', {
			layout : true,
			locals : {
				title : "Login required",
				appName : "andys app.",
				errorMessage : "",
				returnURL : "back"
			}
		});
	});
};

function getUser(prof) {
	console.log("getUser", prof);
	return prof
}
// Simple route middleware to ensure user is authenticated.
// Use this route middleware on any resource that needs to be protected. If
// the request is authenticated (typically via a persistent login session),
// the request will proceed. Otherwise, the user will be redirected to the
// login page.
function andRestrictToLogin(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}
	res.redirect(conf.proxy + 'auth/login?returnURL=' + req.url)
};
exports.andRestrictToLogin = andRestrictToLogin;
