// set config options for express
// a standard config is defined with
// "production" and "development" customizations  
var express = require("express");

exports.config=function(app, basepath) {
	app.configure(function() {
		app.use(express.logger()); 
		app.use(express.responseTime());
		app.use(express.bodyParser());
		app.use(express.methodOverride());
		app.use(express.cookieParser());
		app.enabled('strict routing');
		app.use(express.session({
			secret : 'blah blah'
		}));
		app.use(express.staticCache());
		app.use(basepath+"images/",express.directory(__dirname + '/public/images'));
		app.use(express.favicon(__dirname + '/public/favicon.ico'));
		app.set('view options', {
		    open: '{{',
		    close: '}}'
		});
	});

	app.configure('development', function() {
		//app.use(basepath+"features/", express.static(__dirname + '/public/graphics'));
		app.use(basepath, express.static(__dirname + '/public'));
		app.use(express.errorHandler({
			dumpExceptions : true,
			showStack : true
		}));
	});

	app.configure('production', function() {
		var oneYear = 31557600000;
		app.use(basepath, express.static(__dirname + '/public', {
			maxAge : oneYear
		}));
		app.use(express.errorHandler());
	});
};