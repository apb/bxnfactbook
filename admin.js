// admin
// utilities for basex
// list,info,help
//
var basex = require("basex");
var session = new basex.Session();

function delete_sessions(req, res){
	var cmd="xquery delete node /sessions/session";
	//console.dir(req.headers);
	session.execute(cmd, function(err, r) {
	res.render('basex/index.ejs', {
		layout : true,
		locals : {
			title : "BaseX packages",
			result : r.result
		}
	});
	});
};