xquery version "3.0";
(:~
 : Core utils for an html web framework with basex
 :
 : @author Andy Bunce
 : @project factbase
 :)
 

module namespace app = 'apb.factbook';
import module namespace model="apb.model" ;

declare namespace soutput = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace xlink="http://www.w3.org/1999/xlink";
 
declare variable $app:factbook:=db:open("factbook","factbook.xml");
declare variable $app:schema:=db:open("factbook","schema.xml")/schema;
declare variable $app:version:="0.129";


(:~
 : sequence of items of type "entity" from the database
 : @param entity eg country
 :)
declare function app:items($entity as xs:string)
{
  let $data:=model:locate($app:schema,($entity))
  return model:items($data)
};

(:~
 :  item with id of type ""entity"
 : @param entity eg country
 : @param id eg "f_1234"
 :)
declare function app:items($entity as xs:string
                          ,$id as xs:string)
{
  let $data:=model:locate($app:schema,($entity,$id))
  return model:items($data)  
};

(:~
 :  repeat items with id of type ""entity"
 : @param entity eg country
 : @param id eg "f_1234"
 : @param link  xpath for repeat element 
 :)
declare function app:items($entity as xs:string
                          ,$id as xs:string
                          ,$repeat as xs:string)
{ 
  let $data:=model:locate($app:schema,($entity,$id,$repeat))
  return model:items($data)  
};
 
(:~
: generate table with row per item field
:) 
declare function app:fieldtable($item
                            ,$root as xs:string
                            ,$fields
                            ,$fmap)
 {
  <table  class="table table-condensed table-striped ret" >
  <caption>Properties</caption>
  <thead>
     <tr>
         <th class="r">Property</th><th>Value</th> 
        </tr>
        </thead>
        <tbody>
        { for $field in $fields
        return
         <tr>
           <td class="r">{string($field/@name)}</td>
           {
             app:td($item,$root,false(),$field,$fmap)
             }
         </tr>
       }
       </tbody>
   </table>
 };

(:~
: generate table with row per sequence item
: @param seq the rows
: @param fields colum defs
: @param fmap data access
:) 
declare function app:sequence-to-table3($seq
                                        ,$fields 
                                        ,$fmap
                                        ,$req)
{ 
  let $base:=$req/url/pathname
  let $root:=substring($base,1+string-length($req/basepath))
  return
  <table  class="table table-condensed table-striped ret">

  <caption>{count($seq)} Rows</caption>
  <thead>
     <tr>
         {for $field in $fields
         return <th class="{model:field-cssclass($field)}">
                  <a href="{$root}?sort={$field/@name}">{string($field/@name)}</a>
                </th> 
        }   
        </tr>
        </thead>
        <tbody>
        {for $row in $seq
      
        let $id:=model:id($row)
        let $url2:=$root || "/" || $id
        return
         <tr>
            {for $field in $fields
             return   app:td($row,$url2,true(),$field,$fmap) 
             } 
         </tr>
       }
       </tbody>
   </table>
 };

 
(:~
 : return table td element to display field-def from $item
 :)
declare function app:td($item
                       ,$root as xs:string
                       ,$isList as xs:boolean
                       ,$field as element(field)
                       ,$fmap) as element(td) 
{
     (: path default to name  :)
     let $fn:=map:get($fmap,$field/@name)
     let $data:=$fn($item)
     (: let $data:=trace($data,"££££"|| $field/@name) :)
     let $x :=
         model:render($data
                     ,$root
                     ,$isList 
                     ,$app:schema
                     ,$field)
   
     let $d:=<td class="{model:field-cssclass($field)}">
                { $x   }
             </td>     
     return $d 
};

(:~
: generate crumbs
:)
declare function app:crumbs($loc as xs:string+)
 {
    let $a:=(
            <li>
              <a href="." title="version:?">Home</a>
             <span class="divider">/</span>
             </li>
             
            ,if(count($loc)=1)
            then <li class="active">{$loc[1]}</li>
            else <li>
                    <a href="fb/{$loc[1]}">{$loc[1]}</a>
                     <span class="divider">/</span>
                 </li>
            
           ,if(count($loc)=2)
            then  <li class="active">{$loc[2]}</li> 
            else <li>
                   <a href="fb/{$loc[1]}/{$loc[2]}">{$loc[2]}</a>
                    <span class="divider">/</span>
                 </li>
            
            ,if(count($loc)=3)
            then <li class="active">
                    {$loc[3]}
                  </li> 
            else ()
            )
     return  <ul class="breadcrumb">
                    {$a}
               <ul class="nav pull-right">
                   <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      Views <b class="caret"></b></a>
                     <ul class="dropdown-menu">
                       <li><a href="fb/entity"><i class="icon-tag"></i> XML</a></li>
                    </ul>
                  </li>
               </ul>                
             </ul>
}; 


(:~
: generate table page for entity
: @param sort ([+-])(field)
: @param req request
:)
 declare function app:list-page($entity-name as xs:string
                                ,$sort as xs:string
                                ,$req)
 {
   let $sort:=if (string($sort)!= '') then $sort else "name" 
   let $seq:=app:items($entity-name)
   let $entity:=model:entity($app:schema,$entity-name)
   let $sorted:=model:sort($seq,$sort,$entity)
   let $fmap:=model:access-map($entity)
   let $flds:=model:list-columns($entity)
   let $table:=app:sequence-to-table3($sorted
                                     ,$flds
                                     ,$fmap
                                     ,$req)
  
   let $crumbs:= app:crumbs($entity/@name)

   return map{
              "title":=("All " || $entity/@name),
              "content":=($crumbs,$table)
              }          
 };
 
 (:~
: generate table page for entity
: @param sort ([+-])(field)
: @param repeat schema @link for repeat
: @param req request
:)
 declare function app:repeat-page($entity as xs:string
                                ,$id as xs:string
                                ,$repeat
                                ,$req)
 {
   let $sort:="name" 
           
   let $seq:=app:items($entity,$id,$repeat)
   
   let $schema:=model:entity($app:schema,$repeat)
   let $columns:=model:list-columns($schema)
  (:
   let $seq:=app:sequence-sort($seq,$sort,$schema)
   :)
   let $fmap:=model:access-map($schema)
   let $table:=app:sequence-to-table3($seq
                                     ,$columns
                                     ,$fmap
                                     ,$req)
  
   let $crumbs:= app:crumbs(($entity,$id,$repeat)) 

   let $title:=concat("Repeat ",$schema/@name," for ",$id)  
   return map{
              "title":=$title,
              "content":=($crumbs,$table)
              }          
 }; 
 (:~ 
 : detail page for id
 :)
 declare function app:detail-page($entity as xs:string
                                 ,$id as xs:string
                                 ,$req
                                 ,$url as xs:string)
 {
       let $in:=app:items($entity,$id)
       let $schema:=$app:schema/db/entity[@name=$entity]
       let $fmap:=model:access-map($schema) 
       let $fields:=$schema/attributes/field   
       let $t1:=app:fieldtable($in,$url,$fields,$fmap)
       let $res:=model:xml2html($in)
       let $crumbs:= app:crumbs(($entity,$id)) 
       let $page:=($crumbs,$t1,<br/>,$res)
       (: add located here :) 
       return map{
           "title":=concat($entity," detail"),
           "content":=$page
           }   
 };
