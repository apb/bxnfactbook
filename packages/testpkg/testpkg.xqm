xquery version "3.0";
(:~
 : State for an html web framework with basex
 :
 : @author Andy Bunce
 : @project factbase
 :)
 

module namespace state = '/apb/test';
declare variable $state:log2:=db:open("log2","log.xml");


(:~
 : add log entry for url
 :)
declare updating function state:log($sid as xs:string
                                 ,$url as xs:string)
{
    insert node 
    <hit ts="{current-dateTime()}" sid="{$sid}" url="{$url}"/>
    as last into $state:log2/root
 };
(:~
 : add log entry form request data
 :)
declare updating function state:logreq($req)
{
    insert node 
    <hit ts="{current-dateTime()}" sid="{$req/session}" url="{$req/url/href}"/>
    as last into $state:log2/root
 };
 