xquery version "3.0";
(:~
 : State for an html web framework with basex
 :
 : @author Andy Bunce
 : @project factbase
 :)
 
module namespace state = 'apb.state';

declare variable $state:req external;
declare variable $state:sessions:=db:open("log2","sessions.xml")/sessions;
declare variable $state:users:=db:open("log2","users.xml")/users;

declare function state:base-uri()
{
    state:base-uri($state:req)
 };
(:~
: return base url from request object 
:)
declare function state:base-uri($req)
{
let $host:=($req/headers/x-forwarded-host
            ,$req/headers/host )[1]
 return concat("http://",$host)
 };
 
 (:~
 : add log entry based on the request data
 :)
declare updating function state:logreq2($req)
{
    insert node
        let $hit:= <hit ts="{current-dateTime()}" url="{$req/url/href}"/>
        let $session:= $state:sessions/session[@sid=$req/session]
        return  if($session)
                then $hit
                else <session sid="{$req/session}">
                <agent>{$req/headers/user-agent/string()}</agent>
                          <history>{$hit}</history>      
                     </session>
    as last into
        let $session:= $state:sessions/session[@sid=$req/session]
        return if($session)
               then $session/history
               else $state:sessions
 };
 
(:~
: add user from provider and type
:)
declare updating function state:add-user($name as xs:string,
                                $provider as xs:string,
                                $id as xs:string) 
{
   insert node
    let $count:=count($state:users/user)
    return
        <user name="{$name}" joined="{current-dateTime()}" id="{$count}">
            <provider type="{$provider}" id="{$id}" />
        </user>
   as last into $state:users
   
 };
(:~
: return user from provider and type
:)
declare function state:find-user($provider as xs:string,
                                $id as xs:string) as element(user)
{
   $state:users/user[provider[@type=$provider and @id=$id]]
 }; 