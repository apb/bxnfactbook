import   module namespace state="apb.state";
let $a:=
<json objects="json headers url query">
  <headers>
    <host>192.168.1.8:9001</host>
    <user-agent>Mozilla/5.0 (Ubuntu; X11; Linux i686; rv:9.0.1) Gecko/20100101 Firefox/9.0.1</user-agent>
    <accept>text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8</accept>
    <accept-language>en-gb,en;q=0.5</accept-language>
    <accept-encoding>gzip, deflate</accept-encoding>
    <accept-charset>ISO-8859-1,utf-8;q=0.7,*;q=0.7</accept-charset>
    <referer>http://orlop.webhop.info/factbook/fb/mountain/f0_36938</referer>
    <cookie>__utma=218068296.791793780.1323731594.1328658901.1328737977.120; __utmz=218068296.1323731594.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmb=218068296.8.10.1328737977; __utmc=218068296; connect.sid=D1kL1mKIoVcY985stHzTsIUf.ElnrK%2BpZxLUTLByOUcbPhEVp3vIYjWZULUC%2F1XCY1kE</cookie>
    <max-forwards>10</max-forwards>
    <x-forwarded-for>192.168.1.9</x-forwarded-for>
    <x-forwarded-host>orlop.webhop.info</x-forwarded-host>
    <x-forwarded-server>192.168.1.10</x-forwarded-server>
    <connection>Keep-Alive</connection>
  </headers>
  <url>
    <pathname>/factbook/test</pathname>
    <path>/factbook/test</path>
    <href>/factbook/test</href>
  </url>
  <method>GET</method>
  <query/>
  <basepath>/factbase</basepath>
  <session>D1kL1mKIoVcY985stHzTsIUf.ElnrK+pZxLUTLByOUcbPhEVp3vIYjWZULUC/1XCY1kE</session>
</json>
return state:base-url($a)