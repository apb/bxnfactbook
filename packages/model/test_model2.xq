import module namespace model="apb.model";
declare variable $schema:=db:open("factbook","schema.xml")/schema;
let $url:=("border")
let $loc:=model:locate($schema,$url)
let $data:=model:items($loc)
let $flds:=model:list-columns($loc/entity)
for $item in $data
for $f in $flds
 let $d:= model:fnfield($f)
 let $x:=$d($item)
 let $html:=model:render($x,"/" || $loc/@url,true(),$schema,$f)
return $html