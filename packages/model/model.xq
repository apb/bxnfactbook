xquery version "3.0";
(:~
 : model utils extracting info from model
 : 
 : @author Andy Bunce
 : @project factbase
 :)
 
module namespace model = 'apb.model';
declare variable $model:version:="30mar";
declare variable $model:xml2html:=db:open("factbook","xmlverbatim.xsl");

(:~
: sequence of nodes from database
: @param locate 
:)
declare function model:items($data as element(data)) as node()*
{
   let $db:=$data/@database/string()
   let $doc:=$data/@doc/string()
   let $items:=db:open($db,$doc)
   let $f:=model:evalfn($data/xpath)        
   return $f($items)       
};

(:
: entity from schema
:)
declare function model:entity($schema as element(schema)
                             ,$name as xs:string)
                             as element(entity)
{
  $schema/db/entity[@name=$name]
};

(:~
: location info for path
: @param model the schema
: @param s  parts :entity [:id [:repeat]]
: @return data element with dbname,docname,xpath
: /country/f0_425/border
:)
declare function model:locate($schema as element(schema)
                             ,$s as xs:string+)
                             as element(data)
{
  let $e:=model:entity($schema,$s[1])
  let $url:=string-join($s,"/")
  return switch(count($s))
         case 3 return
            let $field:= $e/attributes/field[@name=$s[3]]
            let $e2:=model:entity($schema,$field/@link)
            let $f2:=$e/attributes/field[@type="id"] (:border has 2 :)
            let $xpath:= if(empty($field))
                         then error(xs:QName('model:idrefs'),"bad")
                         else  $e/@items || 
                                model:filter-xpath($f2,$s[2]) ||
                                model:field-xpath($field)
            return
             <data url="{$url}"
                database="{$e/../@name}"
                doc="{$e/../@doc}">
                <xpath>{string($xpath)}</xpath>
                {$e2}
                </data>    
          case 2 return
            let $key:=$e/attributes/field[@type="id"]
            let $xpath:= $e/@items ||  model:filter-xpath($key,$s[2])
            return
             <data url="{$url}"
                database="{$e/../@name}"
                doc="{$e/../@doc}">
                <xpath>{string($xpath)}</xpath>
                {$e}
                </data>    
          default return
           let $xpath:= $e/@items
           return if(empty($e))
                  then error(xs:QName("model:entity"),"Entity not found: ")
                  else <data url="{$url}"
                            database="{$e/../@name}"
                            doc="{$e/../@doc}">
                            <xpath>{string($xpath)}</xpath>
                            {$e}
                      </data>     
};

(:~
: xpath to select item using key
:)
declare function model:filter-xpath($field as element(field)
                                   ,$id as xs:string)
                                   as xs:string
{
  let $p:= ($field/path, "@" || $field/@name)[1]
  return "["|| $p ||"='" || $id || "']"
};

(:~
: representation of data
: @param @url 
:) 
declare function model:render($data,
                              $url as xs:string
                             ,$isList as xs:boolean
                             ,$model as element(schema)
                             ,$field as element(field))
{
   if(empty($data))
   then ()
   else if($field/@type='id')
   then
     <a href="{$url}">{data($data)}</a>
     
   else if($field/@type='idref' and $field/@link)
   then  
     let $f:=model:idfn($model,$field/@link) 
     let $xp:=$f($data)        
     return model:idlink($xp,"fb/")
    
   else if($field/@type="idrefs"  and $field/@link)
   then
      (: multiple results :)
      if(count($data))
      then
       <a href="{$url}/{$field/@name }">
          {$field/@name || "(" ||count($data)||")"}
       </a>
      else ()
   else if ($data instance of element())
   then $data
   else data($data)
};
 
(:~
: field items for view in entity
: use view if defined
:)
 declare function model:list-columns($schema as element(entity))
 {
   if($schema/view)
   then $schema/attributes/field[@name=tokenize($schema/view, '\s+')]
   else $schema/attributes/field
   
 };
 
(:~
: sort sequence by attribute
: BUG should use fmap
:)
 declare function model:sort($seq
                         ,$sort as xs:string?
                         ,$schema as element(entity))
 {
   
    let $direction:=if(starts-with($sort,"-"))
                    then -1
                    else 1
    let $sort:=if(substring($sort,1,1)=("+","-"))
               then substring($sort,2) 
               else $sort
    let $type:=$schema/attributes/field[@name=$sort]/@type                          
   return 
   if ($direction = 1) 
   then  
        for $row in $seq
        let $sortBy := data($row/@*[name(.) = $sort])
        order by model:cast($sortBy,$type) ascending
        return $row
   else 
         for $row in $seq
         let $sortBy := data($row/@*[name(.) = $sort])
         order by model:cast($sortBy,$type) descending
         return $row
 };
 
(:~
 :  function items for xpath
 :)
declare function model:evalfn($xpath as xs:string)
{ 
   xquery:eval(concat("function($items){$items"
                          ,$xpath
                          ,"}")
                     )
};

(:~
: return function to access field 
:)
declare function model:field-xpath($field as element(field)) as xs:string 
{
(: use ./path if given else @name is attribute :)
 let $path:=($field/path,concat('@',$field/@name))[1] 
 return "/" || $path
};

(:~
: return class to display field 
:)
declare function model:field-cssclass($field as element(field)) as xs:string 
{
    if($field/@type=('integer','float'))
    then "r"
    else ""
};

(:~
: return id for item 
:)
declare function model:id($item) as xs:string 
{
 ($item/@id
 ,$item/@name
 ,"id" || string(db:node-pre($item))
 )[1]
};

(:
: return function to return alink to id for entity
:)
declare function model:idfn($schema
                            ,$en as xs:string){
 let $e:=model:entity($schema,$en)
 let $name:=$e/../@name
 let $doc:=$e/../@doc
 let $id:=$e/@key
 
 return xquery:eval("
 let $db:=db:open('" || $name || "','" || $doc ||"')
 let $en:='" || $en || "/'
 return function($key as xs:string )as xs:string*{
   let $item:=$db" || $e/@items || "[" ||$id ||"=$key] 
   let $label:=string(($item/name,$item/@name)[1])  
   return if(empty($item))
          then ()
          else ($label ,$en || $key)
}")
};

(:~
: make a link from idfn result
:)
declare function model:idlink($idfn as xs:string*,$pre as xs:string){
    if(empty($idfn))
    then ()
    else <a href="{$pre || $idfn[2]}">{$idfn[1]}</a>
};

(:~
: return id for item 
:)
declare function model:label($item ) as xs:string 
{
 string(($item/name,$item/@name)[1])
};

(:~
: return function to access field 
:)
declare function model:fnfield($field as element(field)) 
{
(: use ./path if given else @name is attribute :)
 let $path:=model:field-xpath($field)
 let $cr:=if($field/@type="function")
          then $field/path/string()
          else "$item" ||$path
 (: let $junk:=trace($cr,"..." || $field/@name)  :)      
 return xquery:eval("function($item){" || $cr || "}")  
};


(:~
: data type
:)
declare function model:cast($data, $type as xs:string?)
{
switch ($type)
  case "integer" return xs:integer($data)
  case "float"  return number($data)
  case "dateTime"  return xs:dateTime($data)
  default      return $data
};

(:~
 : return html representation of xml
 :)
 declare function model:xml2html($in)
 {
     if(empty($in))
     then ()
     else xslt:transform($in, $model:xml2html)
 };
(:~
: build map of field access functions
: key is name, data is function to access it
:)
declare function model:access-map($entity as element(entity))
{
    map:new(
     for $fld in $entity/attributes/field 
     return map:entry($fld/@name, model:fnfield($fld))
     )
};



 (:~
: json map of o/p
: e.g. model:json(map {"a":=7,"b":=<p>some text<i>aa</i></p>})
: <json objects="json">
:  <a>7</a>
: <b>&lt;p&gt;some text&lt;i&gt;aa&lt;/i&gt;&lt;/p&gt;</b>
: </json>
:)
 declare function model:json($map as map(*)){
 let $serial:= map { "method" := "html" }
 return <json objects="json">{
  for $m in map:keys($map)
    return element {$m}{serialize($map($m),$serial)}
  }</json>
  };
  
