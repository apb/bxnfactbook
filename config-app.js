// set config options for express
port=9001;
basepath="/factbook/";
var proxy=process.env.PROXY_MOUNT || "http://local-host:"+ port+basepath;

exports.state={
		 port:port
		,basepath: basepath // where to mount
		,proxy:	proxy
		};
