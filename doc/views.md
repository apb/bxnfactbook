# Views
schema.xml

entity.xq
urls:
/factbook/fb/{entity}
app:list-page($entity,$sort,$req ) 
   $seq:=app:items($entity-name) (:sequence of items of type "entity" from the database :)
   $entity:=model:entity($app:schema,$entity-name) (: location info for path :)
   $sorted:=model:sort($seq,$sort,$entity)
   $fmap:=model:access-map($entity)
   $flds:=model:list-columns($entity)
   $table:=app:sequence-to-table3($sorted

/factbook/fb/{entity}/{id}
app:detail-page($entity,$id,$req,$root)

/factbook/fb/{entity}/{id}/{repeat}
app:repeat-page($entity,$id,$repeat,$req)

#bug
model:id assumes seq has @id or @name