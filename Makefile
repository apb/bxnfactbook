ROOT=$(CURDIR)
deploy:
	echo "deploying ................"
	rsync --verbose  --progress --stats --compress  \
      --recursive --times --perms --links --delete \
      --exclude "node_modules/" --exclude "*~" --exclude '.git' \
      * root@velvet:/root/BXNfactbook
xar:
	basex -q $(ROOT)/build/make-xar.xq
	
start:
	node app.js
